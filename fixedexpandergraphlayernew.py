import torch
#from torch.autograd import Variable, Function
import torch.nn as nn
import bipartitegraph
import time
class BipartiteGraphLayer(torch.nn.Module):
    def __init__(self, bipartite_graph):
        super(BipartiteGraphLayer, self).__init__()
        device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.input_features,self.output_features=bipartite_graph.get_number_of_vertices()
        self.number_of_edges=bipartite_graph.get_number_of_edges()
        self.indices_of_edges=bipartite_graph.get_indices_of_edges()


        ##########################Define what shall be trained
        self.weight = nn.Parameter(data=torch.Tensor(self.number_of_edges), requires_grad=True)
        ###########################NO IDEA WHAT IT DOES!
        #nn.init.kaiming_normal(self.weight.data,mode='fan_in')
        #self.indices_of_edges =  nn.Parameter(self.indices_of_edges.cuda())
        ############################I gues this says do not train this.
        self.indices_of_edges.requires_grad = False
        #############################Move Everything to GPU
        #self.indices_of_edges.cuda()
        #self.indices_of_edges.to(device)
        self.weight.cuda()

    #############################Working, but slow?
    def forward(self, input):
        mat=torch.sparse.FloatTensor(self.indices_of_edges,self.weight,torch.Size([self.output_features,self.input_features]))
        mat=mat.to_dense()
        output=torch.matmul(input,mat)
        return output

    #######################DOES NOT WORK SINCE SPARSE MAT MUL DOES NOT WORK
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     mat=torch.sparse.FloatTensor(self.indices_of_edges,self.weight,torch.Size([self.output_features,self.input_features]))
    #     for batch in range(len(input)):
    #         print(input[batch].shape)
    #         print(mat.shape)
    #         output[batch]=torch.sparse.mm(mat,input[batch])
    #     return output

    ##########################SO MUCH LOOPS...
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     for batch in range(input.shape[0]):
    #         for e in range(self.number_of_edges):
    #             m,n=self.indices_of_edges[e]
    #             output[batch][m]=output[batch][m]+self.weight[e]*input[batch][n]
    #    print('it succeded')
    #     return output

    ##########################With one loop
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     for e in range(self.number_of_edges):
    #         m,n=self.indices_of_edges[e]
    #         output[:,m]=output[:,m]+self.weight[e]*input[:,n]
    #     return output

def generate_BipartiteGraphLayer_from_adjacency_matrix(adjacency_matrix):
    bipartite_graph=bipartitegraph.BipartiteGraph(adjacency_matrix,"graph from adjacency matrix")
    return BipartiteGraphLayer(bipartite_graph)