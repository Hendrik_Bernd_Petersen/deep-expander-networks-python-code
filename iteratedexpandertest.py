import random
import numpy
import torch
import torch.nn.functional as F
from torch import nn, optim
from torchvision import datasets, transforms

import time
import matplotlib.pyplot as plt

import fixedexpandergraphlayer
import fixedexpandergraphlayernew
import bipartitegraph

class TestSetup:
    def __init__(self,learning_rate,epochs,input_size,output_size,hidden_exponent,hidden_size,inner_edges_exponent,number_of_inner_edges):
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_exponent = hidden_exponent
        self.hidden_size = hidden_size
        self.inner_edges_exponent = inner_edges_exponent
        self.number_of_inner_edges = number_of_inner_edges
        self.iterations = hidden_size

def perform_test():
    random.seed()
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    print(device)

    #Fetching Data and Normalizing it
    transform = transforms.Compose([transforms.ToTensor(),
                                    transforms.Normalize([0.5], [0.5])])
    training = datasets.FashionMNIST('~/datasets/F_MNIST/',
                                     download=True,
                                     train=True,
                                     transform=transform)
    training_batches = torch.utils.data.DataLoader(training,
                                                   batch_size=64,
                                                   shuffle=True)
    testing = datasets.FashionMNIST('~/datasets/F_MNIST/',
                                    download=True,
                                    train=False,
                                    transform=transform)
    test_batches = torch.utils.data.DataLoader(testing,
                                               batch_size=64,
                                               shuffle=True)

    training_length=len(training_batches)
    test_length=len(test_batches)

    #Preprocessing Data
    preprocessed_train_batches=[]
    for images,labels in training_batches:
        images = images.to(device)
        images=images.view(images.shape[0], -1)
        labels=labels.to(device)
        labels.to(device)
        batch=images,labels
        preprocessed_train_batches.append(batch)
    preprocessed_test_batches=[]
    for images,labels in test_batches:
        images = images.to(device)
        images=images.view(images.shape[0], -1)
        labels=labels.to(device)
        batch=images,labels
        preprocessed_test_batches.append(batch)


    #Learning Parameters
    learning_rate = 0.005
    rows = 1
    epochs = 100                                                #For Debugging: Set to 10 #Normal: 100



    #General network parameters
    input_size = 28*28
    output_size = 10
    hidden_exponent=6                                            #For Debugging: Set to 3 #Normal: 6
    hidden_size=2**hidden_exponent
    inner_edges_exponent=2*hidden_exponent+3                     #For Debugging: Set to 2*hidden_exponent #Normal: 2*hidden_exponent+3 #needs to be >=2*hidden_exponent
    number_of_inner_edges=2**inner_edges_exponent

    train_loss = numpy.zeros((epochs,hidden_exponent))
    test_loss = numpy.zeros((epochs,hidden_exponent))
    train_acc = numpy.zeros((epochs,hidden_exponent))
    test_acc = numpy.zeros((epochs,hidden_exponent))
    legend_labels = numpy.empty(hidden_exponent,dtype='U50')
    test_setup = TestSetup(learning_rate,epochs,input_size,output_size,hidden_exponent,hidden_size,inner_edges_exponent,number_of_inner_edges)

    bipartite_graphs=[]
    for graph_density_iteration in range(hidden_exponent):
        bipartite_graphs.append([])
        print("Starting graph density iteration number " + str(graph_density_iteration) + " of " + str(hidden_exponent-1) + ".")
        edges_per_node_exponent=graph_density_iteration+1
        edges_per_node=2**edges_per_node_exponent
        number_of_layers=2**(inner_edges_exponent-(hidden_exponent+edges_per_node_exponent))
        legend_labels[graph_density_iteration]="D=" + str(edges_per_node) + ", L=" + str(number_of_layers) + "."

        #Build Network
        model_list=[]
        model_list.append(nn.Linear(input_size, hidden_size))
        model_list.append(nn.ReLU())

        for layer_iteration in range(number_of_layers):
            #############################################################Define Biparite Graphs here
            #############################################################Just by calling
            #############################################################temp=bipartitegraph.MyExpander(hidden_size,hidden_size,edges_per_node,edges_per_node)
            temp=bipartitegraph.ExponentialExpander(hidden_size,hidden_size,edges_per_node,edges_per_node)
            #############################################################
            bipartite_graphs[graph_density_iteration].append(temp)
            if hidden_size*edges_per_node!=(bipartite_graphs[graph_density_iteration][layer_iteration].get_number_of_edges()):
                print("Graph should have " + str(hidden_size*edges_per_node) + " edges and has " + str(bipartite_graphs[graph_density_iteration][layer_iteration].get_number_of_edges()) + " edges.")
            #model_list.append(nn.Linear(hidden_size,hidden_size))
            #model_list.append(fixedexpandergraphlayer.ExpanderLinear(temp.get_adjacency_matrix()))
            model_list.append(fixedexpandergraphlayernew.BipartiteGraphLayer(temp))
            model_list.append(nn.ReLU())

        model_list.append(nn.Linear(hidden_size, output_size))
        model_list.append(nn.LogSoftmax(dim=rows))
        model = nn.Sequential(*model_list)
        model.cuda()

        #Set Parameters for Optimization
        criterion = nn.NLLLoss()
        optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

        for epoch in range(epochs):
            tic=time.time()
            if epoch%10 == 0:
                print("Starting epoch " + str(epoch) + " of " + str(epochs-1) + ".")
            running_loss = 0
            running_loss1 = 0
            #####################################Train the network
            for images, labels in preprocessed_train_batches:
                ## Reset the optimizer
                optimizer.zero_grad()                           ##### Sets Gradient to zero

                # forward pass
                output = model.forward(images)

                # Calculating Loss
                loss = criterion(output, labels)
                # back-propagation
                loss.backward()                                 ##### Calculates one Gradient


                # take the next step                            ##### Performs Optimization
                optimizer.step()
            ######################################Test on Train batches
            correct=0
            total=0
            for images, labels in preprocessed_train_batches:
                # forward pass
                output = model.forward(images)

                # Calculating Loss
                loss = criterion(output, labels)
                running_loss += loss.item()

                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
            train_loss[epoch][graph_density_iteration]=running_loss / training_length
            train_acc[epoch][graph_density_iteration] =correct/total;
            #############################################Test on Test batches
            correct=0
            total=0
            for images, labels in preprocessed_test_batches:
                # forward pass
                output = model.forward(images)

                # Calculating Loss
                loss = criterion(output, labels)
                running_loss1 += loss.item()

                outputs = model(images)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()
            test_loss[epoch][graph_density_iteration]=running_loss1 / test_length
            test_acc[epoch][graph_density_iteration]=correct/total
            toc=time.time()
            if epoch%10 == 0:
                print('Epochs took ' + str(toc-tic) + ' units of time.')
    return train_loss, test_loss, train_acc, test_acc, legend_labels, bipartite_graphs, test_setup

def plot_results(train_loss, test_loss, train_acc, test_acc, legend_labels):
    plt.rcParams["figure.figsize"] = [8, 8]

    #plot all train_loss in first plot and add legend
    plt.figure(1)
    hidden_exponent=len(legend_labels)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(train_loss[:,graph_density_iteration])
    plt.title('Training Performance')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()
    #plot all test_loss in second plot and add legend
    plt.figure(2)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(test_loss[:,graph_density_iteration])
    plt.title('Testing Performance')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()

    #plot all train_acc in first plot and add legend
    plt.figure(3)
    hidden_exponent=len(legend_labels)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(train_acc[:,graph_density_iteration])
    plt.title('Training Accuracy')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()
    #plot all test_acc in second plot and add legend
    plt.figure(4)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(test_acc[:,graph_density_iteration])
    plt.title('Testing Accuracy')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()

