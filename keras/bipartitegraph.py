import tensorflow as tf
import keras
from keras import backend
import copy

import numpy

##############################Feel free to add your own graphs to the bottom of the list.

class BipartiteGraph(object):
    def __init__(self,adjacency_matrix, graph_label):
        self.adjacency_matrix=adjacency_matrix
        self.graph_label=graph_label
        self.number_of_right_vertices=len(adjacency_matrix)          #=N
        self.number_of_left_vertices=len(adjacency_matrix[0])      #=M
        self.number_of_edges=0
        for n in range(self.number_of_left_vertices):
            for m in range(self.number_of_right_vertices):
                #if(adjacency_matrix[m][n]!=0 and adjacency_matrix[m][n]!=1)
                #    throw error
                if adjacency_matrix[m][n]==1:
                    self.number_of_edges=self.number_of_edges+1
    def get_adjacency_matrix(self):
        return self.adjacency_matrix

    def get_graph_label(self):
        return self.graph_label

    def get_number_of_edges(self):
        return self.number_of_edges

    def get_number_of_vertices(self):
        return self.number_of_right_vertices,self.number_of_left_vertices

    def get_regularity(self):
        left_regularity=numpy.zeros(self.number_of_left_vertices)
        right_regularity=numpy.zeros(self.number_of_right_vertices)

        for n in range(self.number_of_left_vertices):
            for m in range(self.number_of_right_vertices):
                if self.adjacency_matrix[m][n]==1:
                    left_regularity[n]=left_regularity[n]+1
                    right_regularity[m]=right_regularity[m]+1
        return left_regularity, right_regularity

    def is_regular(self):
        left_regularity,right_regularity=self.get_regularity()
        left_regular = True
        right_regular = True
        for n in range(self.number_of_left_vertices - 1):
            if left_regularity[n] != left_regularity[n + 1]:
                left_regular = False
        for m in range(self.number_of_right_vertices - 1):
            if right_regularity[m] != right_regularity[m + 1]:
                right_regular = False
        return left_regular, right_regular

    def get_random_walk_matrix(self):
        random_walk_matrix=copy.deepcopy(self.adjacency_matrix)
        left_regularity,right_regularity=self.get_regularity()
        for n in range(self.number_of_left_vertices):
            for m in range(self.number_of_right_vertices):
                random_walk_matrix[m][n]=random_walk_matrix[m][n]/left_regularity[n]
        return random_walk_matrix

    def get_list_and_numbering_edges(self):
        list_of_edges=[]
        numbering_of_edges=[]
        index=0
        for m in range(self.number_of_right_vertices):
            list_of_edges.append([])
            numbering_of_edges.append([])
            for n in range(self.number_of_left_vertices):
                if self.adjacency_matrix[m][n]==1:
                    list_of_edges[m].append(n)
                    numbering_of_edges[m].append(index)
                    index=index+1
        return list_of_edges,numbering_of_edges

    def get_indices_of_edges(self):
        indices_of_edges=[]
        for n in range(self.number_of_left_vertices):
            for m in range(self.number_of_right_vertices):
                if self.adjacency_matrix[m][n]==1:
                    indices_of_edges.append((m,n))
        return indices_of_edges


    # def get_eigenvalues(self):
    #     random_walk_matrix=self.get_random_walk_matrix()
    #     matrix1=torch.cat([torch.zeros(self.number_of_left_vertices,self.number_of_left_vertices),random_walk_matrix],0)
    #     matrix2=torch.cat([torch.transpose(random_walk_matrix,0,1),torch.zeros(self.number_of_right_vertices,self.number_of_right_vertices)],0)
    #     matrix=torch.cat([matrix1,matrix2],1)
    #     eigenvalues=torch.eig(matrix)
    #     return eigenvalues
    #
    # def get_bad_eigenvalues(self):
    #     random_walk_matrix=self.get_random_walk_matrix()
    #     eigenvalues=torch.eig(random_walk_matrix)
    #     return eigenvalues


class UniformlyDistributedLRBG(BipartiteGraph):
    def __init__(self, number_of_right_vertices, number_of_left_vertices, left_regularity, right_regularity):
        adjacency_matrix = numpy.zeros((number_of_right_vertices, number_of_left_vertices))
        graph_label = "Uniformly Distributed LRBG (Random)"
        perm = numpy.zeros(number_of_right_vertices)
        for m in range(number_of_right_vertices):
            perm[m] = m
        for n in range(number_of_left_vertices):
            perm = tf.random.shuffle()
            for d in range(left_regularity):
                adjacency_matrix[perm[d]][n] = 1
        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)

class UniformlyDistributedRRBG(BipartiteGraph):
    def __init__(self, number_of_right_vertices, number_of_left_vertices, left_regularity, right_regularity):
        adjacency_matrix = numpy.zeros((number_of_right_vertices, number_of_left_vertices))
        graph_label = "Uniformly Distributed RRBG (Random)"
        perm = numpy.zeros(number_of_left_vertices)
        for n in range(number_of_left_vertices):
            perm[n] = n
        for m in range(number_of_right_vertices):
            perm = tf.random.shuffle()
            for d in range(right_regularity):
                adjacency_matrix[m][perm[d]] = 1
        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)

class ExponentialExpander(BipartiteGraph):
    def __init__(self, number_of_right_vertices, number_of_left_vertices, left_regularity, right_regularity):
        adjacency_matrix = numpy.zeros((number_of_right_vertices, number_of_left_vertices))
        graph_label = "Exponential Expander (Deterministic)"
        index = 0
        for n in range(number_of_left_vertices):
            for d in range(left_regularity):
                adjacency_matrix[index][n] = 1
                index= (index+1)%number_of_right_vertices
        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)

class LinearExpander(BipartiteGraph):
    def __init__(self, number_of_right_vertices, number_of_left_vertices, left_regularity, right_regularity):
        adjacency_matrix = numpy.zeros((number_of_right_vertices, number_of_left_vertices))
        graph_label = "Linear Expander (Deterministic)"
        for n in range(number_of_left_vertices):
            for d in range(left_regularity):
                adjacency_matrix[(d+n)%number_of_right_vertices][n]=1
        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)




############################To create an expander of your own style use this:
############################You can very the input of the __init__ function.
############################But a BipartiteGraph needs the following objects:
############################adjacency_matrix the matrix with the property:
############################         adjacency_matrix[m][n]=1 if there is an edge between m and n
############################         adjacency_matrix[m][n]=0 else
############################graph_label a string that uniquely identifies your expander
############################

class MyExpander(BipartiteGraph):
    def __init__(self,number_of_right_vertices,number_of_left_vertices, right_regularity, left_regularity):
        adjacency_matrix = numpy.zeros((number_of_right_vertices, number_of_left_vertices))
        graph_label = ""
        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)

#class MyExpander(BipartiteGraph):
#    def __init__(self,number_of_right_vertices,number_of_left_vertices, right_regularity, left_regularity):
#        adjacency_matrix = keras.backend.zeros(number_of_right_vertices, number_of_left_vertices)
#        graph_label = ""
#        BipartiteGraph.__init__(self, adjacency_matrix, graph_label)