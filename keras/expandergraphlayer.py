
from __future__ import absolute_import, division, print_function, unicode_literals
import tensorflow as tf
import bipartitegraph
import numpy
class SparseLayer(tf.keras.layers.Layer):
    def __init__(self, bipartite_graph):
        super(SparseLayer, self).__init__()
        self.num_outputs,self.input_dim = bipartite_graph.get_number_of_vertices()
        self.number_of_edges = bipartite_graph.get_number_of_edges()
        self.indices_of_edges=bipartite_graph.get_indices_of_edges()

    def build(self, input_shape):
        self.kernel = self.add_weight(name='trainable_parameters', shape=(self.number_of_edges,), dtype=numpy.float32, initializer='uniform', trainable=True,) #maybe try initializer='normal'


    def call(self, input):
        mat=[[0]*self.input_dim]*self.num_outputs
        for e in range(self.number_of_edges):
            m,n=self.indices_of_edges[e]
            mat[m][n]=self.kernel[e]
        mat=tf.convert_to_tensor(mat)
        output=tf.matmul(input, mat)
        return output

    def compute_output_shape(self,input_shape):
        return (input_shape[0], self.num_outputs)




