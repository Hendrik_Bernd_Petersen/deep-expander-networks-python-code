import numpy
import tensorflow as tf
from tensorflow import keras

import random
import matplotlib.pyplot as plt

from keras.layers import Activation

import expandergraphlayer
import bipartitegraph

class TestSetup:
    def __init__(self,learning_rate,epochs,input_size,output_size,hidden_exponent,hidden_size,inner_edges_exponent,number_of_inner_edges):
        self.learning_rate = learning_rate
        self.epochs = epochs
        self.input_size = input_size
        self.output_size = output_size
        self.hidden_exponent = hidden_exponent
        self.hidden_size = hidden_size
        self.inner_edges_exponent = inner_edges_exponent
        self.number_of_inner_edges = number_of_inner_edges
        self.iterations = hidden_size

def perform_test():
    random.seed()
    number_of_epochs=10
    number_of_graph_density_iterations=1
    train_loss=numpy.zeros((number_of_graph_density_iterations,number_of_epochs))
    test_loss=numpy.zeros((number_of_graph_density_iterations,number_of_epochs))
    train_acc=numpy.zeros((number_of_graph_density_iterations,number_of_epochs))
    test_acc=numpy.zeros((number_of_graph_density_iterations,number_of_epochs))
    legend_labels = numpy.empty(number_of_graph_density_iterations,dtype='U50')
    ######################################### Load Data
    fashion_mnist = keras.datasets.fashion_mnist
    (train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

    ########################################Preprocess Datta
    input_shape=train_images.shape
    train_images=train_images.reshape((input_shape[0],input_shape[1]*input_shape[2]))
    train_images = train_images / 255.0
    input_shape=test_images.shape
    test_images=test_images.reshape((input_shape[0],input_shape[1]*input_shape[2]))
    test_images = test_images / 255.0
    #train_length=input_shape[0]
    #test_length=input_shape[0]

    for graph_density_iteration in range(number_of_graph_density_iterations):
        print('Starting graph density iteration ' + str(graph_density_iteration) + '.')

        legend_labels[graph_density_iteration]='Tutorial'
        bipartite_graph = bipartitegraph.ExponentialExpander(64,64,64,64)
        sparse_layer = expandergraphlayer.SparseLayer(bipartite_graph)
        #sparse_layer = expandergraphlayer.SparseLayer(64)
        ################################Generate Network
        model = keras.Sequential()
        model.add(keras.layers.Dense(64))
        model.add(Activation('relu'))
        #model.add(sparse_layer)
        model.add(keras.layers.Dense(64))
        model.add(Activation('relu'))
        model.add(keras.layers.Dense(10))
        model.add(Activation('softmax'))
        #print(model)
        ################################ Choose optimizer parameters.
        model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])


        ############################### Perform Optimization
        for epoch in range(number_of_epochs):
            print('Starting epoch ' + str(epoch) +'.')


            ####################### Train Network
            model.fit(train_images, train_labels, epochs=1)

            ######################## Calculates Training Loss and Accuracy
            temp=model.evaluate(train_images,train_labels)
            train_loss[graph_density_iteration][epoch] = temp[0]
            train_acc[graph_density_iteration][epoch] = temp[1]

            ######################## Calculates Testing Loss and Accuracy
            temp=model.evaluate(test_images,test_labels)
            test_loss[graph_density_iteration][epoch] = temp[0]
            test_acc[graph_density_iteration][epoch] = temp[1]
    test_setup=0
    bipartite_graphs=0
    return train_loss, test_loss, train_acc, test_acc, legend_labels, bipartite_graphs, test_setup

def plot_results(train_loss, test_loss, train_acc, test_acc, legend_labels):
    plt.rcParams["figure.figsize"] = [8, 8]

    #plot all train_loss in first plot and add legend
    plt.figure(1)
    hidden_exponent=len(legend_labels)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(train_loss[graph_density_iteration])
    plt.title('Training Performance')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()
    #plot all test_loss in second plot and add legend
    plt.figure(2)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(test_loss[graph_density_iteration])
    plt.title('Testing Performance')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()

    #plot all train_acc in first plot and add legend
    plt.figure(3)
    hidden_exponent=len(legend_labels)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(train_acc[graph_density_iteration])
    plt.title('Training Accuracy')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()
    #plot all test_acc in second plot and add legend
    plt.figure(4)
    for graph_density_iteration in range(hidden_exponent):
        plt.plot(test_acc[graph_density_iteration])
    plt.title('Testing Accuracy')
    plt.ylabel('Loss')
    plt.xlabel('Epochs')
    plt.legend(legend_labels, loc='best')
    plt.draw()
    plt.show()

