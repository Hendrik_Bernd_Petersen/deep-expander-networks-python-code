import torch
#from torch.autograd import Variable, Function
import torch.nn as nn
import bipartitegraph
import time
class BipartiteGraphLayer(torch.nn.Module):
    def __init__(self, bipartite_graph):
        super(BipartiteGraphLayer, self).__init__()
        device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
        self.input_features,self.output_features=bipartite_graph.get_number_of_vertices()
        self.number_of_edges=bipartite_graph.get_number_of_edges()
        self.indices_of_edges=bipartite_graph.get_indices_of_edges()


        ##########################Define what shall be trained
        self.weight = nn.Parameter(data=torch.Tensor(self.output_features, self.input_features), requires_grad=True)
        self.adjacency_matrix=bipartite_graph.get_adjacency_matrix()
        self.adjacency_matrix=self.adjacency_matrix.cuda()
        nn.init.kaiming_normal_(self.weight.data,mode='fan_in')
        self.adjacency_matrix =  nn.Parameter(self.adjacency_matrix.cuda())
        self.adjacency_matrix.requires_grad = False

    #############################Working, but slow?
    def forward(self, input):
        #self.save_for_backward(input, weight)
        extendWeights = self.weight.clone()
        extendWeights.mul_(self.adjacency_matrix.data)
        output = input.mm(extendWeights.t())
        return output

    #######################DOES NOT WORK SINCE SPARSE MAT MUL DOES NOT WORK
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     mat=torch.sparse.FloatTensor(self.indices_of_edges,self.weight,torch.Size([self.output_features,self.input_features]))
    #     for batch in range(len(input)):
    #         print(input[batch].shape)
    #         print(mat.shape)
    #         output[batch]=torch.sparse.mm(mat,input[batch])
    #     return output

    ##########################SO MUCH LOOPS...
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     for batch in range(input.shape[0]):
    #         for e in range(self.number_of_edges):
    #             m,n=self.indices_of_edges[e]
    #             output[batch][m]=output[batch][m]+self.weight[e]*input[batch][n]
    #    print('it succeded')
    #     return output

    ##########################With one loop
    # def forward(self, input):
    #     device_cuda=input.device
    #     dtype_temp=input.dtype
    #     output=torch.zeros(input.shape,dtype=dtype_temp,device=device_cuda)
    #     for e in range(self.number_of_edges):
    #         m,n=self.indices_of_edges[e]
    #         output[:,m]=output[:,m]+self.weight[e]*input[:,n]
    #     return output

def generate_BipartiteGraphLayer_from_adjacency_matrix(adjacency_matrix):
    bipartite_graph=bipartitegraph.BipartiteGraph(adjacency_matrix,"graph from adjacency matrix")
    return BipartiteGraphLayer(bipartite_graph)


class Flatten(nn.Module):
    def forward(self, input):
        return input.view(input.size(0), -1)