import torch
import torchvision
import torchvision.transforms as transforms
import matplotlib.pyplot as plt

import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import bipartitegraph
import bipartitegraphLayer

import numpy as np

def generate_network_and_optimization_problem(hidden_size,edges_per_node,number_of_layers):
    learning_rate=0.001
    momentum=0.9
    network_list=[]
    bipartite_graph=bipartitegraph.LinearExpander(hidden_size, hidden_size, edges_per_node, edges_per_node)
    network_list.append(bipartitegraphLayer.Flatten())
    network_list.append(nn.Linear(28*28, 64))
    network_list.append(nn.ReLU())
    for k in range(number_of_layers):
        network_list.append(bipartitegraphLayer.BipartiteGraphLayer(bipartite_graph))
        network_list.append(nn.ReLU())
    network_list.append(nn.Linear(64,10))
    network_list.append(nn.ReLU())
    network = nn.Sequential(*network_list)
    network.cuda()

    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(network.parameters(), lr=learning_rate, momentum=momentum)
    return network,bipartite_graph,criterion,optimizer,learning_rate,momentum

def generate_parameters():
    epochs=20
    batch_size=4
    return epochs,batch_size

def _train_and_test(network, criterion, optimizer, train_loader, test_loader, batch_size):
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    for i, data in enumerate(train_loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = network(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

    train_loss = 0
    train_acc = 0
    for i, data in enumerate(train_loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        # forward
        outputs = network(inputs)
        loss = criterion(outputs, labels)
        train_loss = train_loss + loss.item()

        # Acc
        trash, predict = torch.max(outputs, 1)
        non_zero = torch.nonzero(labels - predict)
        train_acc = train_acc + non_zero.shape[0]
    train_acc = 1 - train_acc / (batch_size * len(train_loader))

    test_loss = 0
    test_acc = 0
    for i, data in enumerate(test_loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        inputs = inputs.to(device)
        labels = labels.to(device)

        # forward
        outputs = network(inputs)
        loss = criterion(outputs, labels)
        test_loss = test_loss + loss.item()

        # Acc
        trash, predict = torch.max(outputs, 1)
        non_zero = torch.nonzero(labels - predict)
        test_acc = test_acc + non_zero.shape[0]
    test_acc = 1 - test_acc / (batch_size * len(test_loader))

    return train_loss,train_acc,test_loss,test_acc

def _train_and_test_other(network, criterion, optimizer, train_loader, test_loader, batch_size):

    train_loss = 0
    train_acc = 0
    for i, data in enumerate(train_loader, 0):

        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # zero the parameter gradients
        optimizer.zero_grad()

        # forward + backward + optimize
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        train_loss=train_loss+loss.item()


        trash, predict=torch.max(outputs,1)
        non_zero=torch.nonzero(labels-predict)
        train_acc=train_acc+non_zero.shape[0]


        loss.backward()
        optimizer.step()

    train_acc=1-train_acc/(batch_size*len(train_loader))

    test_loss = 0
    test_acc = 0
    for i, data in enumerate(test_loader, 0):

        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data

        # forward
        outputs = net(inputs)
        loss = criterion(outputs, labels)
        test_loss=test_loss+loss.item()

        # Acc
        trash, predict=torch.max(outputs,1)
        non_zero=torch.nonzero(labels-predict)
        test_acc=test_acc+non_zero.shape[0]
    test_acc=1-test_acc/(batch_size*len(test_loader))

    return train_loss,train_acc,test_loss,test_acc



def load_data(batch_size):
    transform = transforms.Compose(
        [transforms.ToTensor(),
         transforms.Normalize((0.5,), (0.5,))])
    # datasets
    train_set = torchvision.datasets.FashionMNIST('./data',
                                                 download=True,
                                                 train=True,
                                                 transform=transform)
    test_set = torchvision.datasets.FashionMNIST('./data',
                                                download=True,
                                                train=False,
                                                transform=transform)
    # dataloader
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size,
                                              shuffle=True, num_workers=2)

    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size,
                                             shuffle=False, num_workers=2)

    return train_loader,test_loader

def plot_loss_and_acc(train_loss, train_acc, test_loss, test_acc, epochs, layers, densities):
    axis=np.arange(1,epochs+1)
    #train_loss=train_loss / np.max(train_loss)
    #test_loss=test_loss / np.max(test_loss)
    hidden_exponent=len(layers)
    labels=np.empty((hidden_exponent), dtype=np.dtype('U25'))
    for k in range(hidden_exponent):
        labels[k]='L=' + str(layers[k]) + ', D=' + str(densities[k])

    plt.figure(figsize=(20,20))
    plt.subplot(221)
    for k in range(hidden_exponent):
        plt.plot(axis,train_loss[k])
    plt.legend(labels)
    plt.xlabel('epochs')
    plt.ylabel('train loss')

    plt.subplot(222)
    for k in range(hidden_exponent):
        plt.plot(axis,train_acc[k])
    plt.legend(labels)
    plt.xlabel('epochs')
    plt.ylabel('train acc')

    plt.subplot(223)
    for k in range(hidden_exponent):
        plt.plot(axis,test_loss[k])
    plt.legend(labels)
    plt.xlabel('epochs')
    plt.ylabel('test loss')

    plt.subplot(224)
    for k in range(hidden_exponent):
        plt.plot(axis,train_acc[k])
    plt.legend(labels)
    plt.xlabel('epochs')
    plt.ylabel('test acc')



def perform_test():
    epochs,batch_size=generate_parameters()
    train_loader,test_loader=load_data(batch_size)

    hidden_exponent=6
    hidden_size=2**hidden_exponent

    train_loss=np.zeros((hidden_exponent,epochs))
    train_acc=np.zeros((hidden_exponent,epochs))
    test_loss=np.zeros((hidden_exponent,epochs))
    test_acc=np.zeros((hidden_exponent,epochs))

    layers=[]
    densities=[]
    bipartite_graphs=[]
    learning_rates=[]
    momentums=[]
    for graph_density_iteration in range(hidden_exponent):
        number_of_layers=2**(graph_density_iteration)
        edges_per_node=2**(hidden_exponent-graph_density_iteration)

        layers.append(number_of_layers)
        densities.append(edges_per_node)

        network,bipartite_graph,criterion,optimizer,learning_rate,momentum=generate_network_and_optimization_problem(hidden_size,edges_per_node,number_of_layers)

        learning_rates.append(learning_rate)
        momentums.append(momentum)
        bipartite_graphs.append(bipartite_graph)

        for epoch in range(epochs):
            temp =_train_and_test(network, criterion, optimizer, train_loader, test_loader, batch_size)
            train_loss[graph_density_iteration,epoch]=temp[0]
            train_acc[graph_density_iteration,epoch]=temp[1]
            test_loss[graph_density_iteration,epoch]=temp[2]
            test_acc[graph_density_iteration,epoch]=temp[3]
            print('Finished epoch ' + str(epoch+1) + ' of ' + str(epochs) + '.')
        print('Finished graph density iteration ' + str(graph_density_iteration+1) + ' of ' + str(hidden_exponent) + '.')
    return train_loss, train_acc, test_loss,test_acc, epochs, layers, densities, bipartite_graphs, learning_rates,momentums
